#!/bin/bash

# This script uses dpkg-deb to build the common-libs-meta package and name it according to the
# version set in its control file

cd "$(dirname "$0")"

if [ ! -e "common-libs-meta/DEBIAN/control" ] ; then
	echo "ERROR: Could not find 'common-libs-meta' package source directory"
	exit 1
fi

VER=$(grep -e '^Version:' "common-libs-meta/DEBIAN/control")
VER=$(echo ${VER:8})

echo "Read package version: '$VER'"
if [ "$VER" == "" ] || [[ "$VER" =~ [[:blank:]] ]] ; then
	echo "ERROR: Package version not set or set incorrectly!"
	exit 1
fi

dpkg-deb --root-owner-group --build common-libs-meta common-libs-meta-$VER.deb
