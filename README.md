# Common libraries meta-package for Linux Mint

This is a meta-package for Linux Mint 21.x to quickly and easily install a selection of commonly required packages, to allow more things installed outside of the system repositories to "just work" without having to manually identify & install lots of dependencies.

These packages include those specified in Linux Mint's ia32-libs meta-package, Wine's dependencies (note: Wine itself will not be installed), as well as various other packages required by native Linux games and other software.

### Installation

Simply download the .deb package from the [releases page](https://gitlab.com/adamhm/common-libs-meta/-/releases) and double-click on it to install, like any other loose .deb package.

This package might also work on Ubuntu 22.04 LTS and other distros that use it as a base but I have not tested that.

NOTE: Due to a packaging bug upstream with Ubuntu, it is currently impossible to install both the 64- and 32-bit versions of libopusfile0 which libsdl2-mixer depends on (and which is one of the packages this meta-package will attempt to install)... this bug was fixed with version 0.12 of libopusfile0 with Ubuntu 22.10 (kinetic) but at the time of writing (Feb 2024) it is still unfixed on Ubuntu 22.04/Mint 21.x.

To work around this you can try manually downloading and installing the newer fixed versions (both amd64 and i386 versions) from later versions of Ubuntu; however be warned that these may or may not be compatible with the Ubuntu 22.04 package base. These packages can be downloaded from https://packages.ubuntu.com (I have been using the packages from Ubuntu 22.10 without issue but they are no longer available; if later versions don't work properly then I'll see about uploading these somewhere)

**DO NOT attempt to install this meta-package without installing updated libopusfile0 packages first as the system will not notice the conflict until after it tries to install them, resulting in the package being broken and causing some inconvenience**

To build the .deb package from the repository, simply run the makedeb.sh script.
